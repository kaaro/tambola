## Tambola Game
Based on Tambola, often refered to as Housie Game.

## Extra
Will be sharing development on stream as well. [Twitch](https://twitch.tv/karx01)


## Flow Talk

```
Refer image/scan
```
![photo_2020-05-04_000106-1](./assets/images/2020-05-04_000106-1.jpg)
![photo_2020-05-04_000106-2](./assets/images/2020-05-04_000106-2.jpg)
![photo_2020-05-04_000106-3](./assets/images/2020-05-04_000106-3.jpg)
![photo_2020-05-04_000106-4](./assets/images/2020-05-04_000106-4.jpg)
![photo_2020-05-04_000106-5](./assets/images/2020-05-04_000106-5.jpg)


## Development Talk

We use sass for styling, use the following: 

```
    sass --watch .\bg-styles.scss:.\style.css
```

The deployment is done using Netlify.

[![Netlify Status](https://api.netlify.com/api/v1/badges/f06b7d08-d173-4471-9b29-3ba5ecbc44b4/deploy-status)](https://app.netlify.com/sites/building-game/deploys)


